﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Credit Cards" Type="Folder">
			<Item Name="Credit Cards.lvclass" Type="LVClass" URL="../Credit Cards/Credit Cards.lvclass"/>
			<Item Name="Bronze.lvclass" Type="LVClass" URL="../Bronze/Bronze.lvclass"/>
			<Item Name="Silver.lvclass" Type="LVClass" URL="../Silver/Silver.lvclass"/>
			<Item Name="Gold.lvclass" Type="LVClass" URL="../Gold/Gold.lvclass"/>
		</Item>
		<Item Name="Offers" Type="Folder">
			<Item Name="Hotel Offer.lvclass" Type="LVClass" URL="../Hotel Offer/Hotel Offer.lvclass"/>
			<Item Name="Offer.lvclass" Type="LVClass" URL="../Offer/Offer.lvclass"/>
			<Item Name="Fuel Offer.lvclass" Type="LVClass" URL="../Fuel Offer/Fuel Offer.lvclass"/>
		</Item>
		<Item Name="Runner.vi" Type="VI" URL="../Runner.vi"/>
		<Item Name="Dependencies" Type="Dependencies"/>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
